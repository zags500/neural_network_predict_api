from setuptools import setup, find_packages

try:
    # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:
    # for pip <= 9.0.3
    from pip.req import parse_requirements


def load_requirements(fname):
    reqs = parse_requirements(fname, session="test")
    return [str(ir.req) for ir in reqs]


setup(
    name='prediction',
    author='Zadok Storkey',
    version='0.0.0',
    description='Azure functions package for getting data from an azure hosted neural network.',
    packages=find_packages(),
    install_requires=load_requirements("requirements.txt")
)
