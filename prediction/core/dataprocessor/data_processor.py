import logging
from abc import ABC, abstractmethod
import azure.functions as func

from ..datamodels.inputs_collection import InputsCollection
from ..datamodels.outputs_collection import OutputsCollection
from ..datamodels.retrieved_data import RetrievedData
from ..datamodels.return_data import ReturnData
from ..result.result import Result


class DataProcessor(ABC):
    """This class handles the processing of the data before and after the request to the neural network. It is an
    abstract class so there will be subclasses for each implementation."""

    @abstractmethod
    def preprocess(self, data_to_process: RetrievedData) -> Result[InputsCollection]:
        """Take the data which has been taken from the database and process it ready to be sent to the neural network."""
        pass

    @abstractmethod
    def postprocess(self, data_to_process: OutputsCollection,
                    original_retrieved_data: RetrievedData,
                    original_inputs_collection: InputsCollection) -> Result[ReturnData]:
        """Take the data returned from the neural network and process it ready for returning to the user."""
        pass
