import logging
import azure.functions as func
import os

from .calloption.call_option import CallOption
from ..config.main_config import AzureMainConfig, MainConfig, LocalMainConfig
from .datamodels.ml_workspace_info import MLWorkspaceInfo
from .datamodels.neural_network_info import NeuralNetworkInfo
from .datamodels.sql_server_info import SQLServerInfo
from .datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from .dataretriever.data_retriever import DataRetriever
from .datatransfer.data_transfer_object import DataTransferObject
from .neuralnetworkrequester.neural_network_requester import NeuralNetworkRequester
from .predictor import Predictor
from .result.result import Result, ErrorType


def main(req: func.HttpRequest) -> func.HttpResponse:
    """This function is called directly when the api gets sent a request. It contains all the top level logic for handling
    calls to the api."""
    try:
        # Get the json body of the request and deserialize it into a DataTransferObject
        json_body = req.get_body()
        logging.info("Request received with json body: \n" + json_body.decode())
        dto_creation_response : Result[DataTransferObject] = DataTransferObject.create_from_json(json_body)

        # If this fails, respond to the users request by saying that it has failed and why
        if not dto_creation_response.is_success:
            logging.warning("Failed to parse JSON.")
            return http_response_from_result(dto_creation_response)

        # Create a predictor object from the config
        logging.debug("Succeeded in parsing JSON.")
        predictor: Predictor = create_predictor(dto_creation_response.value)

        # Run the predictor with the parameters sent with the request
        result = predictor.run(SQLStoredProcedureCallData(dto_creation_response.value.call_option.stored_procedure_name,
                                                          dto_creation_response.value.sql_stored_procedure_parameters))

        # Respond to the users request with the result
        return http_response_from_result(result)

    # If at any point an exception is thrown and it is not caught and handled by the rest of the code, tell the user
    # that this is the case. (The final version shouldn't give the details of that exception to the user for security
    # reasons, but at the moment it is useful for testing)
    except Exception as e:
        logging.warning("Hit unexpected exception whilst handling request.")
        return http_response_from_result(Result.failure([type(e).__name__ + ": " + str(e)],
                                                        ErrorType.SERVER_ERROR))


def http_response_from_result(result: Result) -> func.HttpResponse:
    """This converts a Result object to a HttpResponse for the purpose of sending it back to the user."""
    response_status_code: int = http_status_code_from_result(result)
    response_body: str = result.to_json()
    logging.info("Responding to request with status code " + str(response_status_code) + ". Response body: \n" + response_body)
    return func.HttpResponse(body=response_body.replace(" ", ""), status_code=response_status_code,
                             mimetype='application/json')


def http_status_code_from_result(result: Result) -> int:
    """This determines what sort of HTTP status code should be given based on a given Result."""
    if result.is_success:
        return 200
    elif result.error_type == ErrorType.USER_ERROR:
        return 400
    else:
        return 500


def create_predictor(data_transfer_object: DataTransferObject) -> Predictor:
    """This creates a predictor and all the objects it is composed of based on the info in the config."""
    # Get the config
    config: MainConfig = get_config()

    # Create the components which make up a Predictor
    sql_server_info: SQLServerInfo = config.sql_server_info
    ml_workspace_info: MLWorkspaceInfo = config.ml_workspace_info
    data_retriever: DataRetriever = DataRetriever(sql_server_info)
    call_option: CallOption = data_transfer_object.call_option
    data_processor = call_option.data_processor
    neural_network_info: NeuralNetworkInfo = call_option.azure_neural_network_info if is_local_azure_instance() else call_option.local_neural_network_info
    neural_network_requester: NeuralNetworkRequester = NeuralNetworkRequester(ml_workspace_info, neural_network_info)

    # Create the predictor and return it
    predictor: Predictor = Predictor(data_retriever, data_processor, neural_network_requester)
    return predictor


def get_config() -> MainConfig:
    """This returns the correct config based on whether this is being deployed on the cloud or locally."""
    return AzureMainConfig() if is_local_azure_instance() else LocalMainConfig()


def is_local_azure_instance():
    """This returns true if this is deployed in the cloud and false otherwise."""
    return os.environ.get("WEBSITE_INSTANCE_ID") is not None
