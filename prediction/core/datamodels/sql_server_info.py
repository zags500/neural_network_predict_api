class SQLServerInfo:
    """This class defines the information needed to access an SQL server."""

    def __init__(self, access_string: str):
        """Instantiate this class from an access string."""
        self.access_string = access_string

    def __eq__(self, other):
        """Two SQLServerInfos are the same if the access string is the same."""
        if isinstance(other, SQLServerInfo):
            return self.access_string == other.access_string
        return False
