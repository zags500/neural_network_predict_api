class SQLStoredProcedureParameters:
    """This class defines the information needed to call a stored procedure on an SQL database."""

    def __init__(self, stored_procedure_parameters: dict):
        """Instantiate this class from the name of the stored procedure and its parameters."""
        self.stored_procedure_parameters = stored_procedure_parameters

    def __eq__(self, other):
        """Two SQLStoredProcedureParameters are equal if their parameters properties are equal."""
        if isinstance(other, SQLStoredProcedureParameters):
            return self.stored_procedure_parameters == other.stored_procedure_parameters
        return False
