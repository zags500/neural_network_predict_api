class InputsCollection:
    """This class defines data that will be put into a neural network."""

    def __init__(self, inputs_collection: list):
        """Instantiate this class from a list of lists of input values."""
        self.inputs_collection = inputs_collection

    def __eq__(self, other):
        """Two of these are the same if the value of inputs_collection is the same."""
        if isinstance(other, InputsCollection):
            return self.inputs_collection == other.inputs_collection
        return False

