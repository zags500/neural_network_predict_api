from .sql_stored_procedure_parameters import SQLStoredProcedureParameters


class SQLStoredProcedureCallData:
    """This class defines the information needed to call a stored procedure on an SQL database."""

    def __init__(self, procedure_name: str, stored_procedure_parameters: SQLStoredProcedureParameters):
        """Instantiate this class from the name of the stored procedure and its parameters."""
        self.procedure_name = procedure_name
        self.stored_procedure_parameters = stored_procedure_parameters

    def __eq__(self, other):
        """Two SQLStoredProcedureCallDatas are equal if all of their properties are equal."""
        if isinstance(other, SQLStoredProcedureCallData):
            return (self.procedure_name == other.procedure_name
                    and self.stored_procedure_parameters == other.stored_procedure_parameters)
        return False
