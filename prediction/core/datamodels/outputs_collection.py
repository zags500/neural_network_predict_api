class OutputsCollection:
    """This class defines data that have been taken out of a neural network."""

    def __init__(self, outputs_collection: list):
        """Instantiate this class from a list of lists of output values."""
        self.outputs_collection = outputs_collection

    def __eq__(self, other):
        """Two of these are the same if the value of outputs_collection is the same."""
        if isinstance(other, OutputsCollection):
            return self.outputs_collection == other.outputs_collection
        return False
