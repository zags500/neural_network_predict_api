class ReturnData:
    """This class defines data that is ready to be returned to the user."""

    def __init__(self, return_data: list, column_headings: list):
        """Instantiate this from a list of column headings and a list of rows of data."""
        self.return_data = return_data
        self.column_headings = column_headings

    def __eq__(self, other):
        """Two ReturnData objects are equal if they have the same headings and contents."""
        if isinstance(other, ReturnData):
            return self.return_data == other.return_data and self.column_headings == other.column_headings
        return False
