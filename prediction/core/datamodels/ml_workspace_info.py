class MLWorkspaceInfo:
    """This class defines the information needed to access an azure machine learning workspace."""

    def __init__(self, ml_workspace_name: str, subscription_id: str, resource_group: str):
        """Instantiate this class from the name of the workspace, the subscription id, and the name of the resource
        group the workspace is in."""

        self.ml_workspace_name = ml_workspace_name
        self.subscription_id = subscription_id
        self.resource_group = resource_group

    def __eq__(self, other):
        """Two MLWorkspaceInfos are equal if all of their properties are equal."""
        if isinstance(other, MLWorkspaceInfo):
            return (self.ml_workspace_name == other.ml_workspace_name and self.subscription_id == other.subscription_id and
                    self.resource_group == other.resource_group)
        return False
