class NeuralNetworkInfo:
    """This class defines the information needed to access an azure hosted neural network."""
    def __init__(self, is_uri: bool, service_name: str, service_uri: str):
        """Instantiate this class from whether it is defined by a uri, the service name if there is one, and the service
        uri if there is one."""
        self.is_uri = is_uri
        self.service_name = service_name
        self.service_uri = service_uri

    def __eq__(self, other):
        if isinstance(other, NeuralNetworkInfo):
            """Two NeuralNetworkInfos are the same if either they are both defined by a uri and their uris are the same or
            they are both defined by a service name and their service names are the same."""
            if self.is_uri != other.is_uri:
                return False
            elif self.is_uri:
                return self.service_uri == other.service_uri
            else:
                return self.service_name == other.service_name
        return False
