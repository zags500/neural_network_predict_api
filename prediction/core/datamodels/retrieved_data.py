class RetrievedData:
    """This class defines data that has been taken from the database."""

    def __init__(self, retrieved_data: list, column_headings: list):
        """Instantiate this from a list of column headings and a list of rows of data."""
        self.retrieved_data = retrieved_data
        self.column_headings = column_headings

    def __eq__(self, other):
        """Two RetrievedData objects are equal if they have the same headings and contents."""
        if isinstance(other, RetrievedData):
            return self.retrieved_data == other.retrieved_data and self.column_headings == other.column_headings
        return False
