import json
from abc import ABC, abstractmethod
from enum import Enum
from typing import TypeVar, Generic, List, Type, Union

from ..datamodels.inputs_collection import InputsCollection
from ..datamodels.outputs_collection import OutputsCollection
from ..datamodels.retrieved_data import RetrievedData
from ..datamodels.return_data import ReturnData

#Required for generic classes
T = TypeVar("T")


class ErrorType(Enum):
    """This enum defines the different kinds of error you can have as far as the results are concerned."""
    USER_ERROR = 1   # User error means the problem is caused by the user doing something wrong
    SERVER_ERROR = 2 # Server error means the problem is caused by something external to the user


class Result(Generic[T]):
    """This is a generic class which defines the result of an operation. It has 4 properties. 'is_success' contains
    whether the operation was successful. 'value' is only available on successful operations and contains the value
    or values produced by the operation. 'error_messages' and 'error_type' contain are only available on an unsuccessful
    operation and contain the messages of what went wrong and the type of error respectively."""

    def __init__(self, is_success: bool, value: T, error_messages: List[str], error_type: ErrorType):
        """Initialises the result object. It usually wont be called directly as there are helper methods to generate
        specific results."""
        self.is_success = is_success
        self.value = value
        self.error_messages = error_messages
        self.error_type = error_type

    @classmethod
    def success(cls: Type[T], value: T) -> T:
        """Creates a result object for a success."""
        return cls(True, value, None, None)

    @classmethod
    def failure(cls: Type[T], error_messages: List[str], error_type: ErrorType):
        """Creates a result object for a failure."""
        return cls(False, None, error_messages, error_type)

    def to_json(self):
        """Serializes the Response to json for putting in a http response."""
        if self.is_success:
            data: Union[list, dict]
            if type(self.value) is RetrievedData:
                data = {"column_headings": self.value.column_headings, "rows": self.value.return_data}
            elif type(self.value) is InputsCollection:
                data = self.value.inputs_collection
            elif type(self.value) is OutputsCollection:
                data = self.value.outputs_collection
            elif type(self.value) is ReturnData:
                data = {"column_headings": self.value.column_headings, "rows": self.value.return_data}
            else:
                data = self.value
            try:
                return json.dumps({"result": "success", "data": data}, indent=4)
            except (TypeError, OverflowError) as e:
                raise TypeError("Result value is not JSON serializable")
        else:
            return json.dumps({"result": "failure", "errors": self.error_messages}, indent=4)


