import logging
import azure.functions as func
import json
import pyodbc
from pyodbc import Connection, Cursor, Row

from ..datamodels.retrieved_data import RetrievedData
from ..datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from ..datamodels.sql_server_info import SQLServerInfo
from ..result.result import Result


class DataRetriever:
    """This class is responsible for getting the data from the SQL server."""

    def __init__(self, sql_server_info: SQLServerInfo):
        """Instantiate this class based on the details of the SQL server which will be taken from the config."""
        self.sql_server_info = sql_server_info

    def get_data(self, sql_stored_procedure_call_data: SQLStoredProcedureCallData) -> Result[RetrievedData]:
        """Get the data from the SQL server by calling the stored procedure specified in the argument. Return a Result,
        which includes the data got from the server is successful."""

        # Connect to the server and set up a cursor so that the data can be read
        logging.debug("Attempting to connect to SQL.")
        connection: Connection = pyodbc.connect(self.sql_server_info.access_string)
        logging.debug("Successfully connected.")
        cursor: Cursor = connection.cursor()

        # Create the SQL query to send to the database based on sql_stored_procedure call data
        execute_string: str = "EXEC " + sql_stored_procedure_call_data.procedure_name + " "
        parameter_strings: list[str] = []
        for key, value in sql_stored_procedure_call_data.stored_procedure_parameters.stored_procedure_parameters.items():
            key_string: str = "@"+key
            value_string: str
            if type(value) == str:
                value_string = "'" + value + "'"
            elif type(value) == int or type(value) == float:
                value_string = str(value)

            parameter_strings.append(key_string + " = " + value_string)
        combined_parameter_string = ", ".join(parameter_strings)
        execute_string = execute_string + combined_parameter_string + ";"

        # Query the database
        logging.debug("Retrieving data from database by running sql stored procedure: " + execute_string)
        cursor.execute(execute_string)

        # Create a RetrievedData object from the results of the request
        rows: list[Row] = cursor.fetchall()
        row_contents: list[list] = [[row_element for row_element in row] for row in rows]
        column_headings = [column_info[0] for column_info in cursor.description]
        retrieved_data: RetrievedData = RetrievedData(row_contents, column_headings)

        # Return a success with the RetrievedData object
        logging.debug("Data retrieved from database: \n" + json.dumps([column_headings] + row_contents, indent=4))#debug

        return Result.success(retrieved_data)

