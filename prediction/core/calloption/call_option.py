from abc import ABC, abstractmethod

from ..datamodels.neural_network_info import NeuralNetworkInfo
from ..dataprocessor.data_processor import DataProcessor


class CallOption(ABC):
    def __init__(self, data_processor: DataProcessor, stored_procedure_name: str,
                 local_neural_network_info: NeuralNetworkInfo, azure_neural_network_info: NeuralNetworkInfo):
        self.data_processor = data_processor
        self.stored_procedure_name = stored_procedure_name
        self.local_neural_network_info = local_neural_network_info
        self.azure_neural_network_info = azure_neural_network_info
