import logging
import azure.functions as func

from .datamodels.inputs_collection import InputsCollection
from .datamodels.outputs_collection import OutputsCollection
from .datamodels.retrieved_data import RetrievedData
from .datamodels.return_data import ReturnData
from .datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from .dataprocessor.data_processor import DataProcessor
from .dataretriever.data_retriever import DataRetriever
from .neuralnetworkrequester.neural_network_requester import NeuralNetworkRequester
from .result.result import Result


class Predictor:
    """The predictor class is a class containing everything related to actually handling the data."""

    def __init__(self, data_retriever: DataRetriever,
                 data_processor: DataProcessor,
                 neural_network_requester: NeuralNetworkRequester):
        """Creates the predictor from the 3 classes that relate to the different stages of carrying out its task."""
        self.data_retriever = data_retriever
        self.data_processor = data_processor
        self.neural_network_requester = neural_network_requester

    def run(self, sql_stored_procedure_call_data: SQLStoredProcedureCallData) -> Result:
        """This is where the data operations actually happen. It sends the data through each of its classes and returns
        on a success or failure."""
        # Get the data
        retrieved_data_result: Result[RetrievedData] = self.data_retriever.get_data(sql_stored_procedure_call_data)

        # If this fails, return that failure
        if not retrieved_data_result.is_success:
            return retrieved_data_result

        # Preprocess the data
        inputs_collection_result: Result[InputsCollection] = self.data_processor.preprocess(retrieved_data_result.value)

        # If this fails, return that failure
        if not inputs_collection_result.is_success:
            return inputs_collection_result

        # Send the data through the neural network
        outputs_collection_result: Result[OutputsCollection] = self.neural_network_requester.get_inference(
            inputs_collection_result.value)

        # If this fails, return that failure
        if not outputs_collection_result.is_success:
            return outputs_collection_result

        # Postprocess the data
        return_data_result: Result[ReturnData] = self.data_processor.postprocess(outputs_collection_result.value,
                                                                                 retrieved_data_result.value,
                                                                                 inputs_collection_result.value)

        # Regardless of the result, return it
        return return_data_result
