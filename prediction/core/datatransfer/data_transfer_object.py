import json
from typing import Tuple, Union, List, Dict

from ..calloption.call_option import CallOption
from ..datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from ..datamodels.sql_stored_procedure_parameters import SQLStoredProcedureParameters
from ..result.result import Result, ErrorType
from ...config.call_option_config import CallOptionConfig
from ...example.example_call_option import ExampleCallOption
from ...gender.gender_call_option import GenderCallOption


class _classProperty(property):
    """This decorator is a combination of the @classmethod decorator and the @property decorator. It allows you to turn a
    method to a static/class property."""

    def __get__(self, cls, owner):
        return classmethod(self.fget).__get__(None, owner)()


class DataTransferObject:
    """This class is a deserialized representation of the json contents of a request to the api."""

    def __init__(self, version: str, sql_stored_procedure_parameters: dict, call_option: CallOption):
        """The constructor creates the DataTransferObject from the deserialized parameters of the api request."""
        self.version = version
        self.sql_stored_procedure_parameters = sql_stored_procedure_parameters
        self.call_option = call_option

    @classmethod
    def validate_json(cls, obj_json: str) -> Result:
        """This method takes the json body of a request and verifies that it would produce a valid DataTransferObject."""
        # Parse the json
        try:
            obj_dict = json.loads(obj_json)
        except json.JSONDecodeError as e:
            return Result.failure([cls.JSONProblem.invalid_json], ErrorType.USER_ERROR)
        if not isinstance(obj_dict, dict):
            return Result.failure([cls.JSONProblem.non_dictionary_json], ErrorType.USER_ERROR)

        # Set up list of unrecognised keys and list of expected keys
        unrecognised_keys: List[str] = []

        # Set up dictionary of expected typing (key_name, type_expected)
        expected_types: Dict[str, type] = {
            "version": str,
            "call_option": str,
            "arguments": dict
        }

        # Set up list of missing keys
        missing_keys: List[str] = list(expected_types.keys())

        # Set up dictionary of keys with wrong value (key_name, type_found)
        wrong_type_keys: Dict[str, type] = {}

        unrecognised_keys, missing_keys, wrong_type_keys = cls._validate_dictionary_recursive(obj_dict, "",
                                                                                              unrecognised_keys,
                                                                                              missing_keys,
                                                                                              expected_types,
                                                                                              wrong_type_keys)

        json_problems: List[str] = []

        if len(unrecognised_keys) > 0:
            json_problems.append(cls.JSONProblem.unrecognised_keys(unrecognised_keys))

        if len(missing_keys) > 0:
            json_problems.append(cls.JSONProblem.missing_keys(missing_keys))

        for wrong_type_key, wrong_type in wrong_type_keys.items():
            json_problems.append(cls.JSONProblem.wrong_type_key(wrong_type_key, expected_types[wrong_type_key],
                                                                wrong_type))

        if len(json_problems) == 0:
            return Result.success(None)
        else:
            return Result.failure(json_problems, ErrorType.USER_ERROR)

    @classmethod
    def _validate_dictionary_recursive(cls, obj_dict: dict, path: str, unrecognised_keys: List[str],
                                       missing_keys: List[str], expected_types: Dict[str, type],
                                       wrong_type_keys: Dict[str, type]) -> Tuple[List[str], List[str],
                                                                                  Dict[str, type]]:
        """This is a recursive method which checks whether the contents of the dictionary is as expected."""
        for key, value in obj_dict.items():
            key_path: str = path + key
            if key_path not in missing_keys:
                unrecognised_keys.append(key_path)
            else:
                missing_keys.remove(key_path)
                if not type(value) is expected_types[key_path]:
                    wrong_type_keys[key_path] = type(value)
                elif type(value) is dict and any(key_path + "." in string for string in missing_keys):
                    unrecognised_keys, missing_keys, wrong_type_keys = cls._validate_dictionary_recursive(
                        value, key_path + ".", unrecognised_keys, missing_keys, expected_types, wrong_type_keys)
        return unrecognised_keys, missing_keys, wrong_type_keys

    @classmethod
    def create_from_json(cls, obj_json: str) -> Result:
        """This method deserializes the json, converts it to the arguments of the DataTransferObject's constructor,
        creates a DataTransferObject, and returns it."""
        # Validate the json first
        json_validation_result = cls.validate_json(obj_json)
        if not json_validation_result.is_success:
            return Result.failure(json_validation_result.error_messages, ErrorType.USER_ERROR)

        # Create the dictionary from the json
        obj_dict = json.loads(obj_json)

        # Get the version
        version: str = obj_dict["version"]

        # Get the call option
        call_option: CallOption = DataTransferObject._get_call_option_from_name(obj_dict["call_option"])

        # Create the SQL stored procedure parameters
        sql_stored_procedure_parameters: SQLStoredProcedureParameters = SQLStoredProcedureParameters(
            obj_dict["arguments"])

        # Create the DTO
        data_transfer_object: cls = cls(version, sql_stored_procedure_parameters, call_option)

        return Result.success(data_transfer_object)

    @classmethod
    def _get_call_option_from_name(cls, call_option_name: str) -> CallOption:
        """This picks a DataProcessor subclass based on a string with the name of the subclass."""
        try:
            return CallOptionConfig.call_option_dictionary[call_option_name]()
        except KeyError as e:
            raise ValueError("Invalid call option specified")

    class JSONProblem:
        """This class defines a problem with a json string which is intended to represent a DataTransferObject. It is
        only used internally within the DataTransferObject."""

        invalid_json = "Request does not contain valid JSON"

        non_dictionary_json = "JSON does not represent a dictionary"

        @classmethod
        def unrecognised_keys(cls, key_names: List[str]) -> str:
            """Creates the message for unrecognised keys."""
            if len(key_names) == 1:
                return "Unrecognised JSON key: " + key_names[0]
            else:
                return "Unrecognised JSON keys: " + ", ".join(key_names)

        @classmethod
        def missing_keys(cls, key_names: List[str]) -> str:
            """Creates the message for missing keys."""
            if len(key_names) == 1:
                return "Missing JSON key: " + key_names[0]
            else:
                return "Missing JSON keys: " + ", ".join(key_names)

        @classmethod
        def wrong_type_key(cls, key_name: str, expected_type: type, actual_type: type) -> str:
            """Creates the message for if the json value corresponding to a key is the wrong type."""
            problem_string: str = "Value of " + key_name + " is of incorrect type. "
            problem_string += "Expected " + str(expected_type) + ". Found " + str(actual_type)
            return problem_string
