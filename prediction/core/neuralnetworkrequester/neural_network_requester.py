import logging
import json
import azure.functions as func
from azureml.core import Webservice, Workspace
import requests

from ..datamodels.inputs_collection import InputsCollection
from ..datamodels.ml_workspace_info import MLWorkspaceInfo
from ..datamodels.neural_network_info import NeuralNetworkInfo
from ..datamodels.outputs_collection import OutputsCollection
from ..result.result import Result


class NeuralNetworkRequester:
    """This is the class that handles getting the prediction/inference from the neural network based on your data."""

    def __init__(self, workspace_info: MLWorkspaceInfo, neural_network_info: NeuralNetworkInfo):
        """Instantiate this class based on information about the azure_ml workspace and information abut the neural
        network."""
        self.neural_network_info = neural_network_info
        self.workspace_info = workspace_info

    def get_inference(self, node_input_collection: InputsCollection) -> Result[OutputsCollection]:
        """Get the inference from the neural network."""

        # If the neural network is specified by a uri, use that. Otherwise, find it my name in the azureml workspace
        if self.neural_network_info.is_uri:
            scoring_uri = self.neural_network_info.service_uri
        else:
            workspace: Workspace = Workspace.get(name=self.workspace_info.ml_workspace_name,
                                                 subscription_id=self.workspace_info.subscription_id,
                                                 resource_group=self.workspace_info.resource_group)
            neural_network_service = Webservice(workspace, self.neural_network_info.service_name)
            scoring_uri = neural_network_service.scoring_uri

        # Work out the contents of the request
        headers = {'Content-Type': 'application/json'}
        data = "{\"data\": " + json.dumps(node_input_collection.inputs_collection) + "}"
        logging.debug("Sending inputs to neural network: \n" + data)

        # Make the request
        resp = requests.post(scoring_uri, data, headers=headers)
        logging.debug("Response recieved from neural network: \n" + resp.json())

        # Return a request result based on the response (This should probably be multiple lines for more readability)
        return Result.success(OutputsCollection(json.loads(resp.json())["result"]))
