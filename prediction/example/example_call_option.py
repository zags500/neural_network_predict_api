from ..core.calloption.call_option import CallOption
from ..core.datamodels.neural_network_info import NeuralNetworkInfo
from .example_data_processor import ExampleDataProcessor


class ExampleCallOption(CallOption):
    def __init__(self):
        super().__init__(ExampleDataProcessor(), "exampleStoredProcedure",
                         NeuralNetworkInfo(True, None, "http://localhost:8890/score"),
                         NeuralNetworkInfo(False, "exampleNeuralNet", None))
