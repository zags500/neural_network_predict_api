from ..core.datamodels.inputs_collection import InputsCollection
from ..core.datamodels.outputs_collection import OutputsCollection
from ..core.datamodels.retrieved_data import RetrievedData
from ..core.datamodels.return_data import ReturnData
from ..core.dataprocessor.data_processor import DataProcessor
from ..core.result.result import Result



class ExampleDataProcessor(DataProcessor):
    """This is an example DataProcessor Subclass, it takes a table with two values, ID and Count. It then uses the counts
    of each pair of rows to produce a difference between the two and feeds a list of those into the neural network, getting
    back whether it is an increase or a decrease. It then turns the results into a table of Before_ID, After_ID and whether
    or not it is an increase."""

    def preprocess(self, data_to_process: RetrievedData) -> Result[InputsCollection]:
        """Take the data which has been taken from the database and process it ready to be sent to the neural network."""
        if data_to_process.column_headings != ["ID", "Count"]:
            raise ValueError("Incorrect column headings for this processor")

        resultant_inputs_collection = []

        for i in range(len(data_to_process.retrieved_data) - 1):
            before: int = data_to_process.retrieved_data[i][1]
            after: int = data_to_process.retrieved_data[i+1][1]
            resultant_inputs_collection.append([after,before])

        return Result.success(InputsCollection(resultant_inputs_collection))

    def postprocess(self, data_to_process: OutputsCollection,
                    original_retrieved_data: RetrievedData,
                    original_inputs_collection: InputsCollection) -> Result[ReturnData]:
        """Take the data returned from the neural network and process it ready for returning to the user."""
        column_headings = ["Before_ID", "After_ID", "Is_Increase"]
        return_data = []

        for i in range(len(data_to_process.outputs_collection)):
            return_data_row = [
                original_retrieved_data.retrieved_data[i][0],
                original_retrieved_data.retrieved_data[i+1][0],
                True if data_to_process.outputs_collection[i][0] > 0 else False
            ]
            return_data.append(return_data_row)

        return Result.success(ReturnData(return_data, column_headings))
