import os

from ..core.datamodels.ml_workspace_info import MLWorkspaceInfo
from ..core.datamodels.neural_network_info import NeuralNetworkInfo
from ..core.datamodels.sql_server_info import SQLServerInfo


class MainConfig:
    def __init__(self,
                 sql_server_access_string: str,
                 ml_workspace_subscription_id: str,
                 ml_workspace_resource_group: str,
                 ml_workspace_name: str):
        self.sql_server_info = SQLServerInfo(sql_server_access_string)
        self.ml_workspace_info = MLWorkspaceInfo(ml_workspace_name, ml_workspace_subscription_id, ml_workspace_resource_group)


class AzureMainConfig(MainConfig):
    def __init__(self):
        super().__init__(os.environ["sqlServerAccessString"],
                         os.environ["mlWorkspaceSubscriptionID"], os.environ["ml_workspace_resource_group"],
                         os.environ["ml_workspace_name"])


class LocalMainConfig(MainConfig):
    def __init__(self):
        #super().__init__('DRIVER={SQL Server};SERVER=nninferencetest.database.windows.net;DATABASE=nninferencetestgrpcdata;UID=functionsuser;PWD=<Insert Password Here>',
        #    '3eb1d5cf-ed74-4504-8cac-8384c0149ae6', 'ml_resource_group', 'ml_workspace')
        super().__init__('DRIVER={SQL Server};SERVER=localhost,1401;DATABASE=AsuutaLtd_Cloud_Service;UID=SA;PWD=<Insert Password Here>',
            '3eb1d5cf-ed74-4504-8cac-8384c0149ae6', 'ml_resource_group', 'ml_workspace')