from ..example.example_call_option import ExampleCallOption
from ..gender.gender_call_option import GenderCallOption


class CallOptionConfig:
    call_option_dictionary = {
        "example": ExampleCallOption,
        "gender": GenderCallOption
    }
