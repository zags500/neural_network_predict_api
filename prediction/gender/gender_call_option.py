from ..core.calloption.call_option import CallOption
from ..core.datamodels.neural_network_info import NeuralNetworkInfo
from .gender_data_processor import GenderDataProcessor


class GenderCallOption(CallOption):
    def __init__(self):
        super().__init__(GenderDataProcessor(), "getRawJointData",
                         NeuralNetworkInfo(True, None, "http://localhost:8890/score"),
                         NeuralNetworkInfo(False, "genderNeuralNet", None))
