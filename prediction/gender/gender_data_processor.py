from ..core.datamodels.inputs_collection import InputsCollection
from ..core.datamodels.outputs_collection import OutputsCollection
from ..core.datamodels.retrieved_data import RetrievedData
from ..core.datamodels.return_data import ReturnData
from ..core.dataprocessor.data_processor import DataProcessor
from ..core.result.result import Result


class GenderDataProcessor(DataProcessor):

    def preprocess(self, data_to_process: RetrievedData) -> Result[InputsCollection]:
        if data_to_process.column_headings != ["SessionID", "StepNumber", "RawData"]:
            raise ValueError("Incorrect column headings for this processor")
        if len(data_to_process.retrieved_data) < 2:
            raise ValueError("Not enough data in session")

        # This turns a list of:
        # [[<SessionID>, 0, <step 0 data as string>], [<SessionID>, 1, <step 1 data as string>], [<SessionID>, 2, <step 2 data as string>], ...]
        # into:
        # [<step 0 data as string>, <step 1 data as string>, <step 2 data as string>, ...]
        data = [top_level_list[2] for top_level_list in data_to_process.retrieved_data]

        # This turns each:
        # <step n data as string>
        # into a:
        # [<step n frame 0 data as string>, <step n frame 1 data as string>, ...]
        # it does this by splitting the step string at newline characters
        data_split_into_lines = [s.splitlines()[1:] for s in data] # Also need to remove first row of each set of lines

        # This removes the first 16 characters from each <step n frame m data as string> as they are irrelevant
        data_split_into_lines_characters_removed = [[s[16:] for s in row] for row in data_split_into_lines]

        # This turns each:
        # <step n frame m data as string>
        # into a
        # [<step n frame m value 0 as string>, <step n frame m value 1 as string>, ...]
        # it does this by splitting each string on a ',' character
        data_all_strings_split = [[s.split(",") for s in row] for row in data_split_into_lines_characters_removed]

        # This converts each:
        # <step n frame m value i as string>
        # into a
        # <step n frame m value i as float>
        data_no_strings = [[[float(s) for s in subrow] for subrow in row] for row in data_all_strings_split]

        # At this point the full list looks like the following:
        # [
        #   [<list of float values for step 0 frame 0>, <list of float values for step 0 frame 1>, ...],
        #   [<list of float values for step 1 frame 0>, <list of float values for step 1 frame 1>, ...],
        #   [<list of float values for step 2 frame 0>, <list of float values for step 2 frame 1>, ...],
        #   ...
        # ]

        # Initialise an empty list ready to put data into.
        diff_between_every_frame = []

        # Iterate through steps in data
        for i in range(len(data_no_strings)):
            # Iterate through frames in step (there isn't always the same number of frames in a step)
            for j in range(len(data_no_strings[i])):
                if j == len(data_no_strings[i]) - 1:
                    if i == len(data_no_strings) - 1:
                        # If we are at the last frame of the last step we must break because there is nothing to compare it to
                        break
                    else:
                        # If we are at the last frame of a step then we need to compare with the first frame of the next step
                        i2 = i + 1
                        j2 = 0
                else:
                    # If we are not at the last frame of the step just compare it with the next frame
                    i2 = i
                    j2 = j + 1

                # Get the actual values for the start and end
                start = data_no_strings[i][j]
                end = data_no_strings[i2][j2]

                # Get the difference change to go from the start and end, basically turning:
                # [<start1>, <start2>, <start3>, ...] and [<end1>, <end2>, <end3>, ...]
                # to:
                # [<end1 - start1>, <end2 - start2>, <end3 - start3>, ...]
                diff_between_every_frame.append([str(end[i] - start[i]) for i in range(60)])

        # Group the data into sets of 60 frames
        final_data = [diff_between_every_frame[i:i+60] for i in range(0, len(diff_between_every_frame)-60, 60)]
            
        # Return a success result with the data
        return Result.success(InputsCollection(final_data))

    def postprocess(self, data_to_process: OutputsCollection,
                    original_retrieved_data: RetrievedData,
                    original_inputs_collection: InputsCollection) -> Result[ReturnData]:
        maleness = 0
        femaleness = 0
        for i in range(len(data_to_process.outputs_collection)):
            if data_to_process.outputs_collection[i][0] > data_to_process.outputs_collection[i][1]:
                maleness = maleness + 1
                # print('Male ',inferred[i])
            else:
                femaleness = femaleness + 1
                # print('Female ',inferred[i])
        male_est = maleness / (maleness + femaleness) * 100
        female_est = femaleness / (femaleness + maleness) * 100

        return Result.success(ReturnData([[male_est, female_est]], ["Male Estimate", "Female Estimate"]))
