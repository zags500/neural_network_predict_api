import unittest
from unittest.mock import Mock

from prediction.core.predictor import Predictor
from prediction.core.result.result import Result

from tests.core.core_test_data.core_test_data import CoreTestData


class UnitTestPredictor(unittest.TestCase):
    """Tests for the classes in 'prediction/predictor.py. The only class that needs to be tested in this file is the
    Predictor class."""

    def setUp(self):
        """Set up the tests_old by creating a mock DataRetriever, a mock ExampleDataProcessor and a mock
        NeuralNetworkRequester and then feeding them all into the constructor of the Predictor class."""
        self.mock_data_retriever = Mock()
        self.mock_data_retriever.get_data = Mock(return_value=Result.success(CoreTestData.test_retrieved_data))
        self.mock_example_data_processor = Mock()
        self.mock_example_data_processor.preprocess = Mock(return_value=Result.success(CoreTestData.test_inputs_collection))
        self.mock_neural_network_requester = Mock()
        self.mock_neural_network_requester.get_inference = Mock(return_value=Result.success(CoreTestData.test_outputs_collection))
        self.mock_example_data_processor.postprocess = Mock(return_value=Result.success(CoreTestData.test_return_data))

        self.predictor = Predictor(self.mock_data_retriever,
                                   self.mock_example_data_processor,
                                   self.mock_neural_network_requester)

    def test_data_retriever_get_data_called_correctly(self):
        """Test that when you call predictor.run with the correct data, DataRetriever.get_data is called."""
        self.predictor.run(CoreTestData.test_sql_stored_procedure_call_data)
        self.mock_data_retriever.get_data.assert_called_with(CoreTestData.test_sql_stored_procedure_call_data)

    def test_example_data_processor_preprocess_called_correctly(self):
        """Test that when you call predictor.run with the correct data, ExampleDataProcessor.preprocess is called."""
        self.predictor.run(CoreTestData.test_sql_stored_procedure_call_data)
        self.mock_example_data_processor.preprocess.assert_called_with(CoreTestData.test_retrieved_data)

    def test_example_neural_network_requester_get_inference_called_correctly(self):
        """Test that when you call predictor.run with the correct data, NeuralNetworkRequester.get_inference is called."""
        self.predictor.run(CoreTestData.test_sql_stored_procedure_call_data)
        self.mock_neural_network_requester.get_inference.assert_called_with(CoreTestData.test_inputs_collection)

    def test_example_data_processor_postprocess_called_correctly(self):
        """Test that when you call predictor.run with the correct data, ExampleDataProcessor.postprocess is called."""
        self.predictor.run(CoreTestData.test_sql_stored_procedure_call_data)
        self.mock_example_data_processor.postprocess.assert_called_with(CoreTestData.test_outputs_collection,
                                                                        CoreTestData.test_retrieved_data,
                                                                        CoreTestData.test_inputs_collection)

    def test_example_return_value_correct(self):
        """Test that when you call predictor.run with the correct data, the correct result is returned."""
        result = self.predictor.run(CoreTestData.test_sql_stored_procedure_call_data)
        assert result.value == CoreTestData.test_return_data
