import unittest
from unittest.mock import Mock
from unittest.mock import patch

from prediction.core.dataretriever.data_retriever import DataRetriever
from tests.core.core_test_data.core_test_data import CoreTestData
from tests.example.example_mock_objects.pyodbc_related_mocks import ExampleMockConnection, ExampleMockCursor


@patch('pyodbc.connect', new_callable=ExampleMockConnection)
class UnitTestDataRetriever(unittest.TestCase):
    """Tests for the classes in 'prediction/dataretriever/data_retriever.py. The only class that needs to be tested in
    this file is the DataRetriever class."""

    def setUp(self):
        """Create the DataRetriever."""
        self.data_retriever = DataRetriever(
            CoreTestData.test_sql_server_info
        )

    def test_get_data_sends_request(self, mock_cursor: Mock):
        """Assert that get_data calls execute on the cursor with the correct arguments."""
        # Run Function
        self.data_retriever.get_data(
            CoreTestData.test_sql_stored_procedure_call_data
        )

        # Assertions
        ExampleMockCursor.mock_execute.assert_called_with(CoreTestData.test_sql_procedure_query_string)

    def test_get_data_returns_correct_value(self, mock_cursor: Mock):
        """Assert that get_data returns the correct value if the execute method works."""
        # Run Function
        retrieved_data_result = self.data_retriever.get_data(
            CoreTestData.test_sql_stored_procedure_call_data
        )

        # Assertions
        assert retrieved_data_result.is_success
        assert retrieved_data_result.value.column_headings == CoreTestData.test_retrieved_data.column_headings
        assert retrieved_data_result.value.retrieved_data == CoreTestData.test_retrieved_data.retrieved_data
