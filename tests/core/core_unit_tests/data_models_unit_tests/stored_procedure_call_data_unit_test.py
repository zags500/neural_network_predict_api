import unittest
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData


class UnitTestSQLStoredProcedureCallData(unittest.TestCase):
    """Tests for the classes in 'prediction/datamodels/sql_stored_procedure_call_data.py. The only class that
    needs to be tested in this file is the SQLStoredProcedureCallData class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_equality(self):
        """Assert that the definition of equality for SQLStoredProcedureCallData is as expected."""
        # Assert that it is equal to an identical SQLStoredProcedureCallData
        assert (SQLStoredProcedureCallData("T", {"a": 1, "b": "two"}) == SQLStoredProcedureCallData("T", {"a": 1,
                                                                                                          "b": "two"}))
        # Assert that it is not equal to a SQLStoredProcedureCallData that differs in an argument
        assert (SQLStoredProcedureCallData("T", {"a": 1, "b": "two"}) != SQLStoredProcedureCallData("T", {"a": 1,
                                                                                                          "b": "one"}))
        # Assert that it is not equal to a SQLStoredProcedureCallData that differs in procedure name
        assert (SQLStoredProcedureCallData("T", {"a": 1, "b": "two"}) != SQLStoredProcedureCallData("U", {"a": 1,
                                                                                                          "b": "two"}))
        # Assert that it is not equal to a SQLStoredProcedureCallData that differs in procedure name capitalisation
        # (note: I'm not sure this requirement actually makes sense, perhaps it should be the same if it differs in
        # capitalisation as that is how SQL works)
        assert (SQLStoredProcedureCallData("T", {"a": 1, "b": "two"}) != SQLStoredProcedureCallData("t", {"a": 1,
                                                                                                          "b": "two"}))
        # Assert that it is not equal to a SQLStoredProcedureCallData that differs in an argument's capitalisation
        assert (SQLStoredProcedureCallData("T", {"a": 1, "b": "two"}) != SQLStoredProcedureCallData("T", {"a": 1,
                                                                                                          "b": "Two"}))
