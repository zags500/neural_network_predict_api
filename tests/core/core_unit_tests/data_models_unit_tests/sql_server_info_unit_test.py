import unittest
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData


class UnitTestSQLServerInfo(unittest.TestCase):
    """Tests for the classes in 'prediction/datamodels/sql_server_info.py. The only class that
    needs to be tested in this file is the SQLServerInfo class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_equality(self):
        """Assert that the definition of equality for SQLServerInfo is as expected."""
        # Assert that it is equal to an identical SQLServerInfo
        assert (SQLServerInfo("Test") == SQLServerInfo("Test"))
        # Assert that it is not equal to an SQLServerInfo with different values
        assert (SQLServerInfo("Test") != SQLServerInfo("NotTest"))
        # Assert that it is not equal to an SQLServerInfo with values with different capitalisation
        assert (SQLServerInfo("Test") != SQLServerInfo("test"))
        # Assert that it is not equal to a string with one of it's values
        assert (SQLServerInfo("Test") != "Test")
        # Assert that it is not equal to a NeuralNetworkInfo with a value in common
        assert (SQLServerInfo("Test") != NeuralNetworkInfo(False, "Test", None))