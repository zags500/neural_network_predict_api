import unittest
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData


class UnitTestRetrievedData(unittest.TestCase):
    """Tests for the classes in 'prediction/datamodels/retrieved_data.py. The only class that
    needs to be tested in this file is the RetrievedData class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_equality(self):
        """Assert that the definition of equality for RetrievedData is as expected."""
        # Assert that it is equal to an identical RetrievedData
        assert (RetrievedData([[0, 0], [1, 2]], ["A", "B"]) == RetrievedData([[0, 0], [1, 2]], ["A", "B"]))
        # Assert that it is not equal to an RetrievedData with different values
        assert (RetrievedData([[0, 0], [1, 2]], ["A", "B"]) != RetrievedData([[0, 0], [1, 1]], ["A", "B"]))
        # Assert that it is not equal to an list which is the same as its values
        assert (RetrievedData([[0, 0], [1, 2]], ["A", "B"]) != [[0, 0], [1, 2]])
        # Assert that it is not equal to an list which is the same as its column headings
        assert (RetrievedData([[0, 0], [1, 2]], ["A", "B"]) != ["A", "B"])
        # Assert that it is not equal to an ReturnData with the same values
        assert (RetrievedData([[0, 0], [1, 2]], ["A", "B"]) != ReturnData([[0, 0], [1, 2]], ["A", "B"]))