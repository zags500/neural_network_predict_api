import unittest
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData


class UnitTestNeuralNetworkInfo(unittest.TestCase):
    """Tests for the classes in 'prediction/datamodels/neural_network_info.py. The only class that
    needs to be tested in this file is the InputsCollection class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_equality(self):
        """Assert that the definition of equality for NeuralNetworkInfo is as expected."""
        # Assert that if is_uri is false it is equal to an identical NeuralNetworkInfo
        assert (NeuralNetworkInfo(False, "Test", None) == NeuralNetworkInfo(False, "Test", None))
        # Assert that if is_uri is false it is equal to an NeuralNetworkInfo that only differs in uri
        assert (NeuralNetworkInfo(False, "Test", None) == NeuralNetworkInfo(False, "Test", "Test"))
        # Assert that if is_uri is false it is not equal to a NeuralNetworkInfo that differs in service name
        assert (NeuralNetworkInfo(False, "Test", None) != NeuralNetworkInfo(False, "NotTest", None))
        # Assert that if is_uri is false it is not equal to a NeuralNetworkInfo that differs in service name capitalisation
        assert (NeuralNetworkInfo(False, "Test", None) != NeuralNetworkInfo(False, "test", None))
        # Assert that if is_uri is false it is not equal to a string which is the same as its service name
        assert (NeuralNetworkInfo(False, "Test", None) != "Test")
        # Assert that if is_uri is false it is not equal to an SQLServerInfo which has a value the same as its service name
        assert (NeuralNetworkInfo(False, "Test", None) != SQLServerInfo("Test"))
        # Assert that if is_uri is true it is equal to an identical NeuralNetworkInfo
        assert (NeuralNetworkInfo(True, None, "Test") == NeuralNetworkInfo(True, None, "Test"))
        # Assert that if is_uri is true it is equal to an NeuralNetworkInfo that only differs in service name
        assert (NeuralNetworkInfo(True, None, "Test") == NeuralNetworkInfo(True, "Test", "Test"))
        # Assert that if is_uri is false it is not equal to a NeuralNetworkInfo that differs in uri
        assert (NeuralNetworkInfo(True, None, "Test") != NeuralNetworkInfo(True, None, "NotTest"))
        # Assert that if is_uri is false it is not equal to a NeuralNetworkInfo that differs in uri capitalisation
        assert (NeuralNetworkInfo(True, None, "Test") != NeuralNetworkInfo(True, None, "test"))
        # Assert that if is_uri is false it is not equal to a string which is the same as its uri
        assert (NeuralNetworkInfo(True, None, "Test") != "Test")
        # Assert that if is_uri is false it is not equal to an SQLServerInfo which has a value the same as its uri
        assert (NeuralNetworkInfo(True, None, "Test") != SQLServerInfo("Test"))
        # Assert that it is not equal to an NeuralNetworkInfo that differs in is_uri
        assert (NeuralNetworkInfo(False, "Test1", "Test2") != NeuralNetworkInfo(True, "Test1", "Test2"))