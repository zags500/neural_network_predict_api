import unittest
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData


class UnitTestOutputsCollection(unittest.TestCase):
    """Tests for the classes in 'prediction/datamodels/outputs_collection.py. The only class that
    needs to be tested in this file is the OutputsCollection class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_equality(self):
        """Assert that the definition of equality for OutputsCollection is as expected."""
        # Assert that it is equal to an identical OutputsCollection
        assert (OutputsCollection([[0, 0], [1, 2]]) == OutputsCollection([[0, 0], [1, 2]]))
        # Assert that it is not equal to an OutputsCollection with different values
        assert (OutputsCollection([[0, 0], [1, 2]]) != OutputsCollection([[0, 0], [1, 1]]))
        # Assert that it is not equal to an list with the same values
        assert (OutputsCollection([[0, 0], [1, 2]]) != [[0, 0], [1, 2]])
        # Assert that it is not equal to an InputsCollection with the same values
        assert (OutputsCollection([[0, 0], [1, 2]]) != InputsCollection([[0, 0], [1, 2]]))