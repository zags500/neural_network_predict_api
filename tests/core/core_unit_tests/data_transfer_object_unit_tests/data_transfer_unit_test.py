import unittest
from typing import List

from prediction.core.datatransfer.data_transfer_object import DataTransferObject
from prediction.core.result.result import ErrorType, Result
from tests.core.core_test_data.core_test_data import CoreTestData


class UnitTestDataTransferObject(unittest.TestCase):
    """Tests for the classes in 'prediction/datatransfer/data_transfer_object.py. The only class that needs to be tested
     in this file is the DataTransferObject class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_validate_returns_true_when_it_should(self):
        """Test that validate_json returns a success if the json is the correct representation of a DTO"""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json)
        assert result.is_success

    def test_validate_returns_true_regardless_of_whitespace(self):
        """Test that validate_json returns a success if the json is the correct representation of a DTO, regardless of
        the whitespace around the json."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_whitespaced)
        assert result.is_success

    def test_validate_returns_false_for_invalid_json(self):
        """Test that validate_json returns a failure if the json is invalid and produces a result with the correct error
        type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_invalid)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "valid" in result.error_messages[0]

    def test_validate_returns_false_for_json_reperesenting_list(self):
        """Test that validate_json returns a failure when given a json string representing a list and produces a result
        with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_list)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "not represent" in result.error_messages[0]

    def test_validate_returns_false_for_json_reperesenting_string(self):
        """Test that validate_json returns a failure when given a json string representing a string and produces a
        result with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_string)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "not represent" in result.error_messages[0]

    def test_validate_returns_false_for_json_reperesenting_int(self):
        """Test that validate_json returns a failure when given a json string representing an integer and produces a
        result with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_int)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "not represent" in result.error_messages[0]

    def test_validate_returns_false_when_keys_are_missing(self):
        """Test that validate_json returns a failure when given a json string with missing keys and produces a result
        with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_empty)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "Missing" in result.error_messages[0]

    def test_validate_returns_false_when_unrecognised_keys_are_at_top_level(self):
        """Test that validate_json returns a failure when given a json string with top level keys that cannot be
        recognised and produces a result with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_unrecognised_keys_1)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "Unrecognised" in result.error_messages[0]

    # This case is impossible at the moment
    #def test_validate_returns_false_when_unrecognised_keys_are_at_lower_level(self):
        #"""Test that validate_json returns a failure when given a json string with lower level keys that cannot be
        #recognised and produces a result with the correct error type and error message."""
        #result = DataTransferObject.validate_json(TestData.ExampleTestData.test_data_transfer_object_json_unrecognised_keys_2)
        #assert not result.is_success
        #assert result.error_type == ErrorType.USER_ERROR
        #assert len(result.error_messages) == 1 and "Unrecognised" in result.error_messages[0]

    def test_validate_returns_false_when_unrecognised_keys_are_wrong_type(self):
        """Test that validate_json returns a failure when given a json string with keys with values of the incorrect
        datatype and produces a result with the correct error type and error message."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_wrong_type_keys)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 1 and "type" in result.error_messages[0]

    def test_validate_multiple_problems_returns_multiple_error_messages(self):
        """Test that validate_json returns a failure when given a json string with missing keys, unrecognised keys, and
        keys of the wrong datatype and produces a result with the correct error type and error messages."""
        result = DataTransferObject.validate_json(CoreTestData.test_data_transfer_object_json_multiple_problems)
        assert not result.is_success
        assert result.error_type == ErrorType.USER_ERROR
        assert len(result.error_messages) == 3
        assert any("Unrecognised" in problem for problem in result.error_messages)
        assert any("type" in problem for problem in result.error_messages)
        assert any("Missing" in problem for problem in result.error_messages)

    def test_create_produces_correct_object(self):
        """Test that create_from_json produces the correct object when given a correct json string."""
        creation_result: Result[DataTransferObject] = DataTransferObject.create_from_json(
            CoreTestData.test_data_transfer_object_json)
        assert creation_result.is_success
        assert creation_result.value is not None
        assert creation_result.value.version == "0.1.0"
        assert creation_result.value.sql_stored_procedure_parameters == CoreTestData.test_sql_stored_procedure_parameters

    def test_create_errors_correctly_when_given_invalid_json(self):
        """Test that create_from_json returns a failure when given a json string with keys with values of the incorrect
        datatype and produces a result with the correct error type and error message."""
        creation_result = DataTransferObject.create_from_json(
            CoreTestData.test_data_transfer_object_json_multiple_problems)
        assert not creation_result.is_success
        assert creation_result.error_type == ErrorType.USER_ERROR
        error_messages: List[str] = creation_result.error_messages
        assert len(error_messages) == 3
        assert any("Unrecognised" in error_message for error_message in error_messages)
        assert any("type" in error_message for error_message in error_messages)
        assert any("Missing" in error_message for error_message in error_messages)

