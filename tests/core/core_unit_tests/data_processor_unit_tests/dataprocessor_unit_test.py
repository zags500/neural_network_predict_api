import random
import unittest

from prediction.core.dataprocessor.data_processor import DataProcessor
from prediction.example.example_data_processor import ExampleDataProcessor
from prediction.gender.gender_data_processor import GenderDataProcessor


class UnitTestDataProcessor(unittest.TestCase):
    """Tests for the classes in 'prediction/dataprocessor/data_processor.py. The only class that needs to be tested in
    this file is the DataProcessor class."""

    def setUp(self) -> None:
        """No setup needed for these tests_old."""
        pass

    def test_class_cannot_be_instantiated(self):
        """Assert that you cannot instantiate this abstract class."""
        try:
            DataProcessor()
            self.assert_fail()
        except TypeError as e:
            assert ("abstract" in e.args[0])