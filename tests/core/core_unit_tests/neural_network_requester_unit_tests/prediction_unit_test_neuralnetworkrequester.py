import unittest
from unittest.mock import patch

from prediction.core.neuralnetworkrequester.neural_network_requester import NeuralNetworkRequester
from tests.core.core_test_data.core_test_data import CoreTestData
from tests.shared_mock_objects.azureml_related_mocks import MockWebservice, MockWorkspace
from tests.example.example_mock_objects.request_related_mocks import ExampleMockRequestResponse


@patch('prediction.core.neuralnetworkrequester.neural_network_requester.requests.post', new_callable=ExampleMockRequestResponse)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Webservice', new_callable=MockWebservice)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Workspace', new_callable=MockWorkspace)
class UnitTestNeuralNetworkRequester(unittest.TestCase):
    """Tests for the classes in 'prediction/neuralnetworkrequester/neural_network_requester.py. The only class that
    needs to be tested in this file is the NeuralNetworkRequester class."""

    def setUp(self):
        """Create the NeuralNetworkRequester."""
        self.neural_network_requester = NeuralNetworkRequester(CoreTestData.test_ml_workspace_info,
                                                               CoreTestData.test_neural_network_info)

    def test_call_to_get_inference_sends_request(self,
                                                 mock_webservice: MockWebservice,
                                                 mock_workspace: MockWorkspace,
                                                 mock_requests: ExampleMockRequestResponse):
        """Assert that when get_inference is called with the correct arguments the correct request is sent to the
        scoring uri."""
        # Run Function
        self.neural_network_requester.get_inference(CoreTestData.test_inputs_collection)

        # Assertions
        mock_requests.assert_called_with(CoreTestData.test_scoring_uri,
                                         CoreTestData.test_neural_network_request_string,
                                         headers=CoreTestData.test_neural_network_request_headers)

    def test_call_to_get_inference_returns_correct_value(self,
                                                         mock_workspace: MockWorkspace,
                                                         mock_webservice: MockWebservice,
                                                         mock_requests: ExampleMockRequestResponse):
        """Assert that when get_inference is called with the correct arguments and the request returns the correct
        result then the returned value is correct."""
        # Run
        outputs_collection_result = self.neural_network_requester.get_inference(CoreTestData.test_inputs_collection)

        # Assertions
        assert outputs_collection_result.is_success
        assert outputs_collection_result.value == CoreTestData.test_outputs_collection
