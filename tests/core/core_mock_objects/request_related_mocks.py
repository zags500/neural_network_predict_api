from unittest.mock import Mock
from tests.core.core_test_data.core_test_data import CoreTestData


# Functionally the same as ExampleMockRequestResponse in the example section but I wanted to make a separate version for
# each folder so that it is less confusing and they can diverge if needed
class CoreMockRequestResponse(Mock):
    """A mock object representing a requests library RequestResponse object. This is meant to look like the response
    to a REST API request to a neural network."""

    def json(self):
        """Return the relevant test data."""
        return CoreTestData.test_neural_network_response_string
