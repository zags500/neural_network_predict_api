from unittest.mock import Mock

from tests.core.core_test_data.core_test_data import CoreTestData


class MockWorkspace(Mock):
    """A mock object representing an azure workspace object."""
    pass


class MockWebservice(Mock):
    """A mock object representing an azure webservice object."""
    @property
    def scoring_uri(self):
        return CoreTestData.test_scoring_uri