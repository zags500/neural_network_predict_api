import unittest
from unittest.mock import patch

from prediction.core.predictor import Predictor
from prediction.example.example_data_processor import ExampleDataProcessor
from prediction.core.dataretriever.data_retriever import DataRetriever
from prediction.core.neuralnetworkrequester.neural_network_requester import NeuralNetworkRequester
from tests.example.example_test_data.example_test_data import ExampleTestData
from tests.shared_mock_objects.azureml_related_mocks import MockWebservice, MockWorkspace
from tests.example.example_mock_objects.pyodbc_related_mocks import ExampleMockConnection
from tests.example.example_mock_objects.request_related_mocks import ExampleMockRequestResponse


@patch('pyodbc.connect', new_callable=ExampleMockConnection)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.requests.post', new_callable=ExampleMockRequestResponse)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Webservice', new_callable=MockWebservice)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Workspace', new_callable=MockWorkspace)
class IntegrationTestPredictorWithExampleSetup(unittest.TestCase):
    """Integration tests_old for the predictor class. This covers all the logic not related to the web requests."""

    def setUp(self) -> None:
        """Create the DataRetriever, ExampleDataProcessor, and NeuralNetworkRequester and create the Predictor from
        them. Note that in this case none of these are mocks because we are testing everything together."""
        data_retriever = DataRetriever(
            ExampleTestData.test_sql_server_info
        )
        data_processor = ExampleDataProcessor()
        neural_network_info = ExampleTestData.test_neural_network_info
        neural_network_requester = NeuralNetworkRequester(ExampleTestData.test_ml_workspace_info, neural_network_info)
        self.predictor = Predictor(data_retriever,
                                   data_processor,
                                   neural_network_requester)

    def test_outputs(self, mock_connection: ExampleMockConnection,
                     mock_request_response: ExampleMockRequestResponse,
                     mock_webservice: MockWebservice,
                     mock_workspace: MockWorkspace):
        """Assert that Predictor.run returns the correct result."""
        result = self.predictor.run(ExampleTestData.test_sql_stored_procedure_call_data)
        assert result.value == ExampleTestData.test_return_data