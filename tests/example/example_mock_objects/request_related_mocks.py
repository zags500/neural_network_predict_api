from unittest.mock import Mock
from tests.example.example_test_data.example_test_data import ExampleTestData


class ExampleMockRequestResponse(Mock):
    """A mock object representing a requests library RequestResponse object. This is meant to look like the response
    to a REST API request to a neural network."""

    def json(self):
        """Return the relevant test data."""
        return ExampleTestData.test_neural_network_response_string
