import json
import os
import random

from prediction.config.main_config import MainConfig
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.ml_workspace_info import MLWorkspaceInfo
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from prediction.core.datamodels.sql_stored_procedure_parameters import SQLStoredProcedureParameters


class ExampleTestData:
    """Test data which relates to using the original example neural network, database and processing functions"""

    # The expected value of a RetrievedData going into ExampleDataProcessor's preprocess function
    test_retrieved_data = RetrievedData([
        [0, 1],
        [1, 2],
        [2, 3],
        [1, 2],
        [30, 100],
        [32, 57]
    ], [
        "ID",
        "Count"
    ])

    test_bad_column_headings_retrieved_data = RetrievedData([
        "A",
        "B"
    ], [
        [0, 1],
        [1, 2],
        [2, 3],
        [1, 2],
        [30, 100],
        [32, 57]
    ])


    # The expected InputsCollection output from ExampleDataProcessor's preprocess function
    test_inputs_collection = InputsCollection([[2, 1], [3, 2], [2, 3], [100, 2], [57, 100]])

    # The expected InputsCollection input to ExampleDataProcessor's postprocess function
    test_outputs_collection = OutputsCollection([[0.999], [4.34], [-1.47], [50], [-34.5]])

    # The expected OutputsCollection output from ExampleDataProcessor's postprocess function
    test_return_data = ReturnData([
        [0, 1, True],
        [1, 2, True],
        [2, 1, False],
        [1, 30, True],
        [30, 32, False]
    ], [
        "Before_ID",
        "After_ID",
        "Is_Increase"
    ])

    # An SQLServerInfo made from a fake connection string
    test_sql_server_info = SQLServerInfo("DRIVER=<driver>;SERVER=<server>;PORT=1433;DATABASE=<database>;"
                                         "UID=<username>;PWD=<password>")

    # The response the scoring uri would give if it were a real one, used for spoofing an actual response
    test_neural_network_response_string = "{\"result\": " + json.dumps(test_outputs_collection.outputs_collection) + "}"

    # A fake NeuralNetworkInfo
    test_neural_network_info = NeuralNetworkInfo(False, "fake_model", None)

    # A test name for an azure machine learning workspace
    test_ml_workspace_name = 'ml_workspace'

    # A test azure subscription id
    test_subscription_id = '3eb1d5cf-ed74-4504-8cac-8384c0149ae6'

    # A test name for an azure resource group
    test_resource_group = 'ml_resource_group'

    # A test machine learning workspace info constructed from the above 3 values
    test_ml_workspace_info = MLWorkspaceInfo(test_ml_workspace_name,
                                             test_subscription_id,
                                             test_resource_group)

    # A StoredProcedureParameters object
    test_sql_stored_procedure_parameters = SQLStoredProcedureParameters({"parameter1": "argument1",
                                                                      "parameter2": "argument2",
                                                                      "parameter3": "argument3"})

    # A StoredProcedureCallData object for giving to DataRetriever
    test_sql_stored_procedure_call_data = SQLStoredProcedureCallData("exampleStoredProcedure",
                                                                     test_sql_stored_procedure_parameters)

    # A json string for constructing the DTO for the request used with ExampleProcessor
    test_data_transfer_object_json = '{"version":"0.1.0", "call_option":"example", "arguments":{'
    test_data_transfer_object_json += '"parameter1":"argument1", "parameter2":"argument2",'
    test_data_transfer_object_json += '"parameter3":"argument3"}}'






