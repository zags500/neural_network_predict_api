import random
import unittest

from prediction.core.dataprocessor.data_processor import DataProcessor
from prediction.example.example_data_processor import ExampleDataProcessor
from prediction.gender.gender_data_processor import GenderDataProcessor
from tests.example.example_test_data.example_test_data import ExampleTestData


class UnitTestExampleDataProcessor(unittest.TestCase):
    """Tests for the classes in 'prediction/dataprocessor/example_data_processor.py. The only class that needs to be tested in
    this file is the ExampleDataProcessor class."""

    def setUp(self) -> None:
        """Create the ExampleDataProcessor."""
        self.example_data_processor = ExampleDataProcessor()

    def test_preprocess_raises_value_error_on_bad_data(self):
        """Assert that a ValueError will be thrown if preprocess is sent an invalid RetrievedData."""
        try:
            self.example_data_processor.preprocess(ExampleTestData.test_bad_column_headings_retrieved_data)
            self.assert_fail()
        except ValueError as e:
            assert ("column headings" in e.args[0])

    def test_preprocess_returns_correct_result(self):
        """Assert that the correct result is returned from preprocess when valid data is sent."""
        inputs_collection_result = self.example_data_processor.preprocess(ExampleTestData.test_retrieved_data)
        assert inputs_collection_result.is_success
        assert inputs_collection_result.value.inputs_collection == ExampleTestData.test_inputs_collection.inputs_collection

    def test_postprocess_returns_correct_result(self):
        """Assert that the correct result is returned from postprocess when valid data is sent."""
        return_data_result = self.example_data_processor.postprocess(
            ExampleTestData.test_outputs_collection, ExampleTestData.test_retrieved_data,
            ExampleTestData.test_inputs_collection
        )
        assert return_data_result.is_success
        assert return_data_result.value.return_data == ExampleTestData.test_return_data.return_data
        assert return_data_result.value.column_headings == ExampleTestData.test_return_data.column_headings

    # We could probably do with some more tests_old for ExampleDataProcessor