import unittest
from unittest.mock import patch, Mock

import azure.functions as func

from prediction.core.main import main
from prediction.core.result.result import Result
from tests.gender.gender_test_data.gender_test_data import GenderTestData
from tests.shared_mock_objects.azureml_related_mocks import MockWebservice, MockWorkspace
from tests.gender.gender_mock_objects.pyodbc_related_mocks import GenderMockConnection
from tests.gender.gender_mock_objects.request_related_mocks import GenderMockRequestResponse


@patch('pyodbc.connect', new_callable=GenderMockConnection)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.requests.post', new_callable=GenderMockRequestResponse)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Webservice', new_callable=MockWebservice)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Workspace', new_callable=MockWorkspace)
@patch('prediction.core.main.get_config', new=(lambda: GenderTestData.test_config))
class IntegrationTestMainWithGenderSetup(unittest.TestCase):
    """Integration tests_old for the main function. This covers all the logic from the request being recieved to the
    response being sent."""
    def setUp(self) -> None:
        """Create a fake Http request which represents a valid request to the API."""
        self.mock_http_request = Mock(func.HttpRequest)
        self.mock_http_request.get_body = Mock(func.HttpRequest.get_body)
        self.mock_http_request.get_body.return_value = GenderTestData.test_data_transfer_object_json.encode('utf-8')

    def test_outputs(self, mock_connection: GenderMockConnection,
                     mock_request_response: GenderMockRequestResponse,
                     mock_webservice: MockWebservice,
                     mock_workspace: MockWorkspace):
        """Assert that a correct http request coming into main results in a correct HttpResponse being returned."""
        response = main(self.mock_http_request)
        expected = "".join(Result.success(GenderTestData.test_return_data).to_json().split())
        actual = "".join(response.get_body().decode().split())
        assert response is not None
        assert actual == expected
        assert response.status_code == 200