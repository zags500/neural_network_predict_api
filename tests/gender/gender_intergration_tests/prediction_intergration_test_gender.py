import unittest
from unittest.mock import patch

from prediction.gender.gender_data_processor import GenderDataProcessor
from prediction.core.predictor import Predictor
from prediction.core.dataretriever.data_retriever import DataRetriever
from prediction.core.neuralnetworkrequester.neural_network_requester import NeuralNetworkRequester
from tests.shared_mock_objects.azureml_related_mocks import MockWebservice, MockWorkspace
from tests.gender.gender_mock_objects.pyodbc_related_mocks import GenderMockConnection
from tests.gender.gender_test_data.gender_test_data import GenderTestData
from tests.gender.gender_mock_objects.request_related_mocks import GenderMockRequestResponse


@patch('pyodbc.connect', new_callable=GenderMockConnection)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.requests.post', new_callable=GenderMockRequestResponse)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Webservice', new_callable=MockWebservice)
@patch('prediction.core.neuralnetworkrequester.neural_network_requester.Workspace', new_callable=MockWorkspace)
class IntegrationTestPredictorWithGenderSetup(unittest.TestCase):
    """Integration tests_old for the predictor class. This covers all the logic not related to the web requests."""

    def setUp(self) -> None:
        """Create the DataRetriever, GRPCDataProcessor, and NeuralNetworkRequester and create the Predictor from
        them. Note that in this case none of these are mocks because we are testing everything together."""
        data_retriever = DataRetriever(
            GenderTestData.sql_server_info
        )
        data_processor = GenderDataProcessor()
        neural_network_info = GenderTestData.test_neural_network_info
        neural_network_requester = NeuralNetworkRequester(GenderTestData.test_ml_workspace_info,
                                                          neural_network_info)
        self.predictor = Predictor(data_retriever,
                                   data_processor,
                                   neural_network_requester)

    def test_outputs(self, mock_connection: GenderMockConnection,
                     mock_request_response: GenderMockRequestResponse,
                     mock_webservice: MockWebservice,
                     mock_workspace: MockWorkspace):
        """Assert that Predictor.run returns the correct result."""
        result = self.predictor.run(GenderTestData.retrieve_sql_stored_procedure_call_data)
        assert result.value == GenderTestData.test_return_data