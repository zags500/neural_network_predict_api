import json
import os
import random

from prediction.config.main_config import MainConfig
from prediction.core.datamodels.inputs_collection import InputsCollection
from prediction.core.datamodels.ml_workspace_info import MLWorkspaceInfo
from prediction.core.datamodels.neural_network_info import NeuralNetworkInfo
from prediction.core.datamodels.outputs_collection import OutputsCollection
from prediction.core.datamodels.retrieved_data import RetrievedData
from prediction.core.datamodels.return_data import ReturnData
from prediction.core.datamodels.sql_server_info import SQLServerInfo
from prediction.core.datamodels.sql_stored_procedure_call_data import SQLStoredProcedureCallData
from prediction.core.datamodels.sql_stored_procedure_parameters import SQLStoredProcedureParameters


class GenderTestData:
    # Sql server info
    sql_server_info = SQLServerInfo(
        'DRIVER={SQL Server};SERVER=stromohab.cy6br91sdj56.us-east-2.rds.amazonaws.com;DATABASE=AsuutaLtd_Cloud_Service;UID=master;PWD=fakepassword')

    # A StoredProcedureCallData object for giving to DataRetriever
    retrieve_sql_stored_procedure_call_data = SQLStoredProcedureCallData("exampleStoredProcedure",
                                                                         SQLStoredProcedureParameters(
                                                                             {
                                                                                 "SessionID": "18af6bc9-3738-46c2-a545-26333cb8c484"}
                                                                         ))

    # Query to retrieve the joint_data for a session (note the name for the parameter may be incorrect as the
    # example I have seen didn't refer to it by name)
    retrieve_sql_procedure_query_string = "EXEC getRawJointData @SessionID='18af6bc9-3738-46c2-a545-26333cb8c484'"

    # Query to insert the data back into the database, not needed yet but I may as well record it now
    insert_sql_procedure_query_string = 'EXEC insertRawJointData @StepNumber=<StepNumber>, @SessionID=<SessionID>,'
    insert_sql_procedure_query_string += '@RawData=%s <DataToReturn>'

    # __file__ returns the directory the file is in rather than the directory the file is run from
    with open(os.path.join(os.path.dirname(__file__), 'textfiles/gender_retrieved_data.txt'), 'r') as file:
        test_retrieved_data = RetrievedData(json.loads(file.read()), ["SessionID", "StepNumber", "RawData"])
    with open(os.path.join(os.path.dirname(__file__), 'textfiles/gender_inputs_collection.txt'), 'r') as file:
        test_inputs_collection = InputsCollection(json.loads(file.read()))
    with open(os.path.join(os.path.dirname(__file__), 'textfiles/gender_outputs_collection.txt'), 'r') as file:
        test_outputs_collection = OutputsCollection(json.loads(file.read()))
    test_return_data: ReturnData = ReturnData([[71.54811715481172, 28.451882845188287]],
                                              ["Male Estimate", "Female Estimate"])

    # The response the scoring uri would give if it were a real one, used for spoofing an actual response
    test_neural_network_response_string = "{\"result\": " + json.dumps(test_outputs_collection.outputs_collection) + "}"

    # A json string for constructing the DTO for the request used with ExampleProcessor
    test_data_transfer_object_json = '{"version":"0.1.0",'
    test_data_transfer_object_json += '"call_option":"gender",'
    test_data_transfer_object_json += '"arguments":{"SessionID":"18af6bc9-3738-46c2-a545-26333cb8c484"}}'

    # A test data processor name which selects the ExampleDataProcessor
    test_data_processor_name = "GenderDataProcessor"

    # A fake NeuralNetworkInfo
    test_neural_network_info = NeuralNetworkInfo(False, "fake_model", None)

    # A test name for an azure machine learning workspace
    test_ml_workspace_name = 'ml_workspace'

    # A test azure subscription id
    test_subscription_id = '3eb1d5cf-ed74-4504-8cac-8384c0149ae6'

    # A test name for an azure resource group
    test_resource_group = 'ml_resource_group'

    # A test machine learning workspace info constructed from the above 3 values
    test_ml_workspace_info = MLWorkspaceInfo(test_ml_workspace_name,
                                             test_subscription_id,
                                             test_resource_group)

    # A test config constructed from many of the above test objects
    test_config = MainConfig(sql_server_info.access_string, test_ml_workspace_name, test_subscription_id,
                             test_resource_group)

    # What the data will look like when it is retrieved from the database
    @classmethod
    def generate_test_retrieved_data(cls, seed):
        random.seed(seed)
        return RetrievedData([[i, cls._generate_test_retrieved_data_row()] for i in range(240)], [
            "StepNumber",
            "RawData"
        ])

    # Helper method for method above
    @classmethod
    def _generate_test_retrieved_data_row(cls):
        return "\n".join(
            ["?" * 16 + ",".join([str((random.random() - 0.5) * 2 * 600) for i in range(60)]) for j in range(15)])

    # What the data will look like when it is retrieved from the database
    @classmethod
    def generate_test_inputs_collection(cls, seed):
        random.seed(seed)
        return InputsCollection([cls._generate_test_inputs_collection_row() for i in range(240)])

    # Helper method for method above
    @classmethod
    def _generate_test_inputs_collection_row(cls):
        return [[(random.random() - 0.5) * 2 * 10 for i in range(60)] for j in range(15)]

    # What the data coming out of the neural network looks like
    @classmethod
    def generate_test_outputs_collection(cls, seed):
        random.seed(seed)
        return OutputsCollection([[cls._generate_test_outputs_collection_row()] for i in range(240)])

    @classmethod
    def _generate_test_outputs_collection_row(cls):
        return [(random.random() * 2 - 1) for i in range(2)]

    @classmethod
    def generate_test_return_data(cls, seed):
        random.seed(seed)
        return ReturnData([random.random() * 100, random.random() * 100], ["Male Estimate", "Female Estimate"])

    @classmethod
    def generate_test_retrieved_data_from_inputs_collection(cls, seed):
        raise NotImplementedError()
        random.seed(seed)
        return RetrievedData(
            [[i, cls._generate_test_retrieved_data_row_from_inputs_collection_row()] for i in range(240)], [
                "StepNumber",
                "RawData"
            ])

    @classmethod
    def _generate_test_retrieved_data_row_from_inputs_collection_row(cls):
        raise NotImplementedError()
        inputs_collection_row = cls._generate_test_inputs_collection_row()
        return "\n".join(["?" * 16 + ",".join([str(i) for i in row]) for row in inputs_collection_row])

    @classmethod
    def generate_test_return_data_from_outputs_collection(cls, seed):
        outputs_collection = cls.generate_test_outputs_collection(seed)
        maleness = 0
        femaleness = 0
        for outputs_set in outputs_collection:
            if outputs_set[0] > outputs_set[1]:
                maleness += 1
            else:
                femaleness += 1
        male_est = maleness / (maleness + femaleness) * 100
        female_est = femaleness / (femaleness + maleness) * 100
        return ReturnData([male_est, female_est])

