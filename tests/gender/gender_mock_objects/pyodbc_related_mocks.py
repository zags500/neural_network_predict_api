from unittest.mock import Mock

from tests.core.core_test_data.core_test_data import CoreTestData
from tests.gender.gender_test_data.gender_test_data import GenderTestData


class GenderMockConnection(Mock):
    """A mock object representing a pyodbc Connection object."""

    def cursor(self):
        return GenderMockCursor()


class GenderMockCursor:
    """A mock object representing a pyodbc Cursor object."""

    def __init__(self):
        """Creates some rows from the test_retrieved _data, initialises next_row to 0, and makes a new mock called
        mock_execute."""
        self.rows = [GenderMockRow(row) for row in GenderTestData.test_retrieved_data.retrieved_data]
        self.next_row = 0
        GenderMockCursor.mock_execute = Mock()

    def execute(self, sql: str = "", params=None):
        """Calls mock execute with the sql string. It is structured a bit strangely so that I can see what mock_execute
        was called with."""
        GenderMockCursor.mock_execute(sql)
        return self

    def fetchone(self):
        """Fakes the fetchone function by actually fetching on row from its list of rows."""
        if self.next_row < len(self.rows):
            row_to_return = self.rows[self.next_row]
            self.next_row = self.next_row + 1
            return row_to_return
        else:
            return None

    def fetchall(self):
        """Fakes the fetchall function by actually fetching all its rows."""
        rows_to_return = self.rows[self.next_row:len(self.rows)]
        self.next_row = len(self.rows)
        return rows_to_return

    def fetchmany(self, count: int):
        """Fakes the fetchall function by actually fetching a given number of rows."""
        if self.next_row + count > len(self.rows):
            self.fetchall()

        rows_to_return = self.rows[self.next_row:self.next_row + count]
        self.next_row = self.next_row + count
        return rows_to_return

    @property
    def description(self):
        """Fakes the description based of the column headings. Description looks something like this:
        [[heading, <other things>], [heading, <other things>], [heading, <other things>] ,[heading, <other things>]],
        but in our code we only care about Description[i][0] so the others don't need to be spoofed."""
        return [[heading] for heading in GenderTestData.test_retrieved_data.column_headings]


class GenderMockRow:
    """A mock object representing a pyodbc Row object."""

    def __init__(self, data: list):
        """Fills in the rows based on the data."""
        self.row_data = data

    def __iter__(self):
        """Used to allow this to be iterated over (i.e for value in row: <x>)."""
        return iter(self.row_data)

    def __getitem__(self, key):
        """Used to allow this to be indexed (i.e row[i]))."""
        return self.row_data[key]

    @property
    def cursor_description(self):
        """Like a normal description, this just returns the same as the cursor description."""
        return [[heading] for heading in GenderTestData.test_retrieved_data.column_headings]
