from unittest.mock import Mock
from tests.gender.gender_test_data.gender_test_data import GenderTestData


class GenderMockRequestResponse(Mock):
    """A mock object representing a requests library RequestResponse object. This is meant to look like the response
    to a REST API request to a neural network."""

    def json(self):
        """Return the relevant test data."""
        return GenderTestData.test_neural_network_response_string
