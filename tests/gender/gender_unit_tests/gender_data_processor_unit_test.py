import random
import unittest

from prediction.core.dataprocessor.data_processor import DataProcessor
from prediction.example.example_data_processor import ExampleDataProcessor
from prediction.gender.gender_data_processor import GenderDataProcessor
from tests.gender.gender_test_data.gender_test_data import GenderTestData


class UnitTestGenderDataProcessor(unittest.TestCase):
    """Tests for the classes in 'prediction/dataprocessor/gender_data_processor.py. The only class that needs to be tested
    in this file is the GRPCDataProcessor class."""

    def setUp(self) -> None:
        """Create the ExampleDataProcessor."""
        self.seed = random.random()
        self.gender_data_processor = GenderDataProcessor()

    def test_preprocess_returns_correct_result(self):
        """Assert that the correct result is returned from preprocess when valid data is sent."""
        inputs_collection_result = self.gender_data_processor.preprocess(GenderTestData.test_retrieved_data)
        assert inputs_collection_result.is_success
        resultant_value = inputs_collection_result.value.inputs_collection
        test_value = GenderTestData.test_inputs_collection.inputs_collection
        assert len(resultant_value) == len(test_value)
        for i in range(len(resultant_value)):
            assert len(resultant_value[i]) == len(test_value[i])
            for j in range(len(resultant_value[i])):
                assert len(resultant_value[i][j]) == len(test_value[i][j])
                for k in range(len(resultant_value[i][j])):
                    assert type(resultant_value[i][j][k]) == str
                    # Almost equal because we are comparing floats
                    self.assertAlmostEqual(float(resultant_value[i][j][k]), float(test_value[i][j][k]))

    def test_postprocess_returns_correct_result(self):
        """Assert that the correct result is returned from postprocess when valid data is sent."""
        return_data_result = self.gender_data_processor.postprocess(
            GenderTestData.test_outputs_collection, GenderTestData.test_retrieved_data,
            GenderTestData.test_inputs_collection
        )
        assert return_data_result.is_success
        assert return_data_result.value.return_data == GenderTestData.test_return_data.return_data
        assert return_data_result.value.column_headings == GenderTestData.test_return_data.column_headings

    # We could probably do with some more tests_old for GenderDataProcessor